import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

public class CalculatorTest {
    private Calculator c;


    //Namnge minst ett test.
    //Kontrollera ordningen på hur tester körs.
    //Låt en metod använda @ParameterizedTest
    @ParameterizedTest
    @Order(2)
    @DisplayName("Testing Addition of 5")
    @ValueSource(ints = {1, 3, 5, -3, 15, 20000})
    public void testAdd5Tonumber(int ints){
        int fem = 5;
        int sum = fem + ints;

        Assertions.assertEquals(sum, Calculator.Add5Tonumber(ints));
    }

    //Namnge minst ett test.
    //Kontrollera ordningen på hur tester körs.
    //Låt en metod testa en array’s storlek.
    // Minst en metod ska vara private och säkerställas den testas.
    @Test
    @Order(1)
    @DisplayName("Testing Array Length")
    public void testCheckArrayList(){
        int[] testArray = new int[11];
        Assertions.assertEquals(11, c.publicCheckArrayLength(testArray));
    }

    //Låt samtliga test kontrollera så eventuella Exceptions också tas om
    //hand.
    @Test
    @Order(3)
    @DisplayName("Testing Devide by Zero")
    void testDivideByZero(){
        Assertions.assertThrows(ArithmeticException.class, () -> c.divide(1,0));
    }

    // "Använd @BeforeEach och @AfterEach till något." - JUnit - Inlämningsuppgift
    @BeforeEach
    public void initEach(){
        c = new Calculator();
    }

    @AfterEach
    public void cleanUpEach(){
        c = null;
    }

}
